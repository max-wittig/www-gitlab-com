---
layout: markdown_page
title: "Category Vision - Auto Devops"
---

- TOC
{:toc}

## Auto DevOps

Our vision for “Auto DevOps” is to leverage our [single application](https://about.gitlab.com/handbook/product/single-application/) to assist users in every phase of the development and delivery process, implementing automatic tasks that can be customized and refined to get the best fit for their needs.

With the dramatic increase in the number of projects being managed by software teams (especially with the rise of micro-services), it's no longer enough to just craft your code. In addition, you must consider all of the other aspects that will make your project successful, such as tests, quality, security, logging, monitoring, etc. It's no longer acceptable to add these things only when they are needed, or when the project becomes popular, or when there's a problem to address; on the contrary, all of these things should be available at inception.

e.g. “auto CI” to compile and test software based on best practices for the most common languages and frameworks, “auto review” with the help of automatic analysis tools like Code Climate, “auto deploy” based on Review Apps and incremental rollouts on Kubernetes clusters, and “auto metrics” to collect statistical data from all the previous steps in order to guarantee performances and optimization of the whole process. Dependencies and artifacts will be first-class citizens in this world: everything must be fully reproducible at any given time, and fully connected as part of the great GitLab experience.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c).


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=auto%20devops)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/480) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

We want to provide users with more control for Auto DevOps. We should not run Auto DevOps on projects that either don't meet the requirements or cannot be built with Auto DevOps.

[Don't run Auto DevOps when no dockerfile or matching buildpack exists](https://gitlab.com/gitlab-org/gitlab/issues/26655)

The above issue has two upstream dependencies:

* [Add file matching rule to flexible rules](https://gitlab.com/gitlab-org/gitlab/issues/24021)
* [Global `rules` section for pipelines](https://gitlab.com/gitlab-org/gitlab/issues/29654)

## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is [Viable](/direction/maturity/). See the [Auto DevOps viable](https://gitlab.com/groups/gitlab-org/-/epics/1333) epic for more info. Deliverables:

- [Don't run Auto DevOps when no dockerfile or matching buildpack exists](https://gitlab.com/gitlab-org/gitlab-ce/issues/57483)

## Competitive Landscape

While there are "piece-meal" solutions that offer to automate a particular stage, there are no comprehensive tools that offer to address the entire devops lifecycle.

### DeployHQ

DeployHQ offers to "Automatically build and deploy code from your repositories", however, its UX is complex that its deployment targets limited.

[deployhq.com](https://www.deployhq.com)

## Analyst Landscape

There is currently no analyst category that aligns with Auto DevOps.

## Top Customer Success/Sales Issue(s)

[Use Auto DevOps for design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/96)

## Top Customer Issue(s)

[Add support for AWS ECS deployments to Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/38430)

## Top Internal Customer Issue(s)

[Use Auto DevOps for design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/96)

#### Top Vision Item(s) 

- [Disable Auto DevOps at the Group level for gitlab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues/52447) 

- [Composable Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/47234)

- [Don't run Auto DevOps when no dockerfile or matching buildpack exists](https://gitlab.com/gitlab-org/gitlab-ce/issues/57483)

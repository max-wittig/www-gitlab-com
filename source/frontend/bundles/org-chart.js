document.addEventListener('DOMContentLoaded', () => {
  const orgChartContainer = document.querySelector('.org-chart-container');

  if (!orgChartContainer) {
    console.warn("org-chart.js included on a page that doesn't have the org chart on it.");
    return;
  }

  orgChartContainer.querySelectorAll('.node.has-tree').forEach(node => {
    node.addEventListener('click', onNodeClick.bind(null, node), false);
  });
});

function onNodeClick(node) {
  const nextSibling = node.nextElementSibling;

  if (nextSibling && nextSibling.classList.contains('tree')) {
    nextSibling.classList.toggle('hide');
    node.classList.toggle('is-expanded');
  }
}

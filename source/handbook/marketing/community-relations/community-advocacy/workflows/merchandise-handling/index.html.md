---
layout: markdown_page
title: "Merchandise handling"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Daily routine

One of the main community advocate task is to fulfill all the swag orders.

### Swag orders

Advocates should check for the new orders regularly as the orders should be fulfilled and shipped in a timely manner. In order to process these orders you'll have to fulfill them both in Shopify and Printfection.

#### Printfection

Fulfilling in Printfection means that Prinfection will pack and ship the items.

1. Login to Printfection
2. Go to the "Shopify store orders" collection under the campaign tab.
3. Go to the manage tab
4. Make sure that all the orders from Shopify are shown in there and that everything is okay.
5. Press the "place the order button".

That's all, Printfection will handle the rest. 

Note: Printfection also sends the email confirmation to the customer.

#### Shopify

Fulfilling in Shopify serves to send the actual notification that an order has been processed, including the invoice for the customer.

1. Login to Shopify
2. Go to the orders page
3. Make sure to check all the new orders highlighted with the yellow Unfulfilled tag.
4. Click on actions > fulfill selected orders
5. Make sure that the default "send a notification" option is selected and press the fulfill button.

That's all, the customers should receive their confirmations automatically.

### Updating the contributors giveaway sheet 

We are sharing swag giveaway links to the community contributors. Advocates should replenish the links each quarter and update the with the new links [sheet](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit#gid=1578711076).

Here's the instruction on how to get the new links:

1. Login to Printfection.
2. Go to "Community Contributor" giveaway campaign.
3. Click on the Manage tab.
4. Generate new links.
5. Make sure to download the CSV file of the freshly created links only
6. Upload the csv file or copy/paste the new links into the [sheet](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit#gid=1578711076)

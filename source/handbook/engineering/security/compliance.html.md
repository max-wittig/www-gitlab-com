---
layout: markdown_page
title: "Security Compliance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Security Compliance Mission

1. Enable GitLab sales by providing customers information and assurance about our information security program and remove security as a barrier to adoption by our customers.
1. Implement a comprehensive compliance program at GitLab to document and formalize our information security program through independent evaluation.
1. Integrate modern compliance features into the GitLab product to make security compliance easier to implement for our customers.

## Roadmap

Our [internal roadmap](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/roadmap) shows our current and planned projects and the currently defined components of work for each.

Our [Trust Center](/security/), shows our current state of industry compliance, our commitment to retaining the trust of our customers, and the direction GitLab is headed in terms of security compliance.

### A basic overview of our compliance projects in 2019 include:
1. Adopt a security control framework as a basis for GitLab's compliance program
   * [GitLab's security controls](/handbook/engineering/security/sec-controls.html)
1. Build out the documentation associated with each prioritized security control
   * Clicking on each prioritized [security control](/handbook/engineering/security/sec-controls.html) takes you to guidance page
   * These controls were prioritized based on our security compliance needs over the next 12-18 months and include controls related to SOC2 common criteria and PCI
1. Perform a gap analysis to establish a baseline/starting point for each of these prioritized controls
   * This gap analysis is currently underway. For more information on the methodology used for this project, please see the [related project epic](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/epics/63)
1. Perform remediation work to bring each evaluated control to a state of "full compliance" (meaning we believe we would pass an audit of that control)
   * This work will begin in Q3 of FY2020
1. Perform testing of each control to validate that all control processes are operating effectively
   * This work will begin as controls are determined to be operating effectively

## GitLab's Control Framework (GCF)

GitLab has adopted an umbrella control framework that provides compliance with a number of industry compliance requirements and best practices. For information about how we developed this framework and a list of all of our security controls, please see the [security controls handbook page](/handbook/engineering/security/sec-controls.html).

## Continuous Monitoring

The Security Compliance team is responsible for completing the activities which continualy assess the design and operating effectiveness of the controls established by the GCF.

### Gap Analysis

The purpose of a gap analysis is to identify gaps between GCF controls and documented GitLab process. Gap analysis project work is done in a private project due to the sensitive nature of the assessment findings. The project will have an issue for every GCF control in scope for the gap analysis. GitLab's [first gap analysis](https://gitlab.com/gitlab-private/sec-compliance/gcf-gap-analysis) can serve as an example for how future gap analyses can be organized and executed.

### Remediation

The remediation phase fills the gaps identified during the gap analysis and get each in-scope control into a state of audit readiness.

A control is considered to be remediated if:

1. The process addressing the requirements of the control (as defined by the security compliance team) is documented in the GitLab handbook
1. The process documented above is the same process being used by all GitLab team-member involved in operation of the security control
1. The collection of testing evidence that will prove the above 2 points can begin
    *  Testing encompasses both "test of design" (sample of 1) and then if that passes, a test of "operating effectiveness" (random sampling)

GitLab's [first remediation project](https://gitlab.com/groups/gitlab-com/gl-security/compliance/-/epics/30) can serve as an example for how future remediation efforts can be organized and executed.

### Testing

Controls which have been remediated should be tested to see whether the process documented in the GitLab handbook, runbooks, and other sources are followed.

## Contact the Compliance Team

* Email
   * `security-compliance@gitlab.com`
* Tag us in Gitlab
   * `@gitlab-com/gl-security/compliance`
* Slack
   * `@sec-compliance-team`
* [GitLab compliance project](https://gitlab.com/gitlab-com/gl-security/compliance/compliance)

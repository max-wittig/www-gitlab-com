---
layout: markdown_page
title: "Business Systems: Portal"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Contacts

[Business Systems Administrator \| Phil Encarnacion](/job-families/finance/business-system-analyst/)

[Business Systems Analyst \| Jamie Carey](/job-families/finance/business-system-analyst/)

## Incoming Customer Inquiries: Licensing, Billing, Transactions
* [Troubleshooting subscription and licensing problems](/handbook/support/workflows/license_troubleshooting.html)
* [License documentation](https://docs.gitlab.com/ee/user/admin_area/license.html) 

### Common Customer Inquiries first triaged by BSS

Business Systems Specialist:
*  User wants to trial a plan other than Gold on GitLab.com (BSS first triages as appropriate, then passes to .com Support [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request)
*  User receives an error during the purchasing process within the customers portal, initially triaged by BSS, then if needed Fulfillment
*  User wants the red renewal approaching banner message in their Self-Managed system removed, customer needs to upload working license
*  User doesn't know the steps to purchase a GitLab.com subscription
*  User doesn't see their group during purchase process, they need to create their group or associate their GitLab.com account with their portal account. If there's an error, send to .com Support
*  User doesn't understand how true-up works
*  Instructions for activating the license key
*  User wants to know when they will receive the license key
*  User doesn't renew paid Self-Managed plan, what happens to the license and features, BSS triages with initial explanation and also cc's Sales
*  A customer reports problems when registering their license key

inquiries routed to Sales:
> If the person is already a customer, the BSS emails the Account Executive. If the person is not a customer, the BSS asks the SDR Regional leads for EMEA, APAC and AMER/LATAM in SFDC chatter group `ZD-newbusiness` who should be cc-ed in an email for the inquiry.
Regional Leads for SDRs: 
>   *  AMER/LATAM - Mona Elliot
>   *  APAC - Jay Thomas-Burrows
>   *  EMEA - Elsje Smart
*  User reports an inability to upgrade from one paid plan to another, cc Account Owner on reply to customer + internal note asking BSS to let BSS know they got it and can close case
*  User is on a trial and wants to purchase a paid plan, cc Account Owner on reply to customer + internal note asking BSS to let BSS know they got it and can close case
*  Customer wants to pay by anything other than a credit card, cc Account Owner on reply to customer + internal note asking BSS to let BSS know they got it and can close case

inquiries routed to .com Support:
*  User wants to trial a plan other than Gold on GitLab.com (BSS first triages as appropriate, then passes to .com Support [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request)
*  User wants to extend GitLab.com trial [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request) 

inquiries routed to Accounts Receivable: (Billing Requests)
*  User wants to downgrade subscription, queue in Zendesk
*  Copy of invoice
*  Changes to invoice (address, company name, VAT #, etc)
*  Refund requests
*  Requests to make a payment/payment failed

## High Level Setup

[video of custom setup](https://drive.google.com/drive/folders/1kfCEQM6XYGWYxq3Ke4TNvtmDR-46erVD)

### Customer Flow

<img src="/handbook/business-ops/images/Customer_flow1.png" class="full-width">

### System Integrations

Important to know

* [READ ME customer portal repo](https://gitlab.com/gitlab-org/customers-gitlab-com#subscription-portal-app)
* API should send country codes ISO 2 from portal to SFDC and Marketo

<img src="/handbook/business-ops/images/portal_integration3.png" class="full-width">

### Zuora and Salesforce

<img src="/handbook/business-ops/images/zuoraSFDC1.png" class="full-width">

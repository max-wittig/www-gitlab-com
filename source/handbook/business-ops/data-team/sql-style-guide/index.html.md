---
layout: markdown_page
title: "SQL Style Guide"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

----

## SQL Style Guide 

**Since we don't have a linter, it is *our collective responsibility* to enforce this Style Guide.**

### General Guidelines

##### Field Naming Conventions

*    Field names should all be lowercased. 

*    An `id`, `name`, or generally ambiguous value such as `type` should always be prefixed by what it is identifying or naming

     ```sql
     -- Good
     SELECT 
       id    AS account_id,
       name  AS account_name,
       type  AS account_type,
       ...
     
     -- Bad
     SELECT 
       id,
       name,
       type,
       ...
     
     ```

*    When joining to any data from a different source, a field should be prefixed with the data source, e.g. `sfdc_account_id`, to avoid ambiguity

     ```sql
     -- Good
     SELECT
       sfdc_account.account_id   AS sfdc_account_id,
       zuora_account.account_id  AS zuora_account_id
     FROM sfdc_account
     LEFT JOIN zuora_account ON ...
     
     -- Bad
     SELECT
       sfdc_account.account_id,
       zuora_account.account_id  AS zuora_id
     FROM sfdc_account
     LEFT JOIN zuora_account ON ...
     ```

*   All field names should be [snake-cased](https://en.wikipedia.org/wiki/Snake_case)

     ```sql
     -- Good
     SELECT
       dvcecreatedtstamp AS device_created_timestamp
     FROM table
     
     -- Bad
     SELECT
       dvcecreatedtstamp AS DeviceCreatedTimestamp
     FROM table
     ```

*   Boolean field names should start with `has_`, `is_`, or `does_`

     ```sql
     -- Good
     SELECT
       deleted AS is_deleted,
       sla     AS has_sla
     FROM table
     
     -- Bad
     SELECT
       deleted,
       sla
     FROM table
     ```

**Dates**
- Timestamps should end with `_at`, e.g. `deal_closed_at`, and should always be in UTC
- Dates should end with `_date`, e.g. `deal_closed_date`
- Months should be indicated as such and should always be truncated to a date format, e.g. `deal_closed_month`
- Always avoid key words like `date` or `month` as a column name
- Prefer the explicit date function over `date_part`, but prefer `date_part` over `extract`, e.g. `DAYOFWEEK(created_at)` > `DATE_PART(dayofweek, 'created_at')` > `EXTRACT(dow FROM created_at)`
  - Note that selecting a date's part is different from truncating the date. `date_trunc('month', created_at)` will produce the calendar month ('2019-01-01' for '2019-01-25') while `SELECT date_part('month', '2019-01-25'::date)` will produce the number 1
 
##### Use CTEs (Common Table Expressions), not subqueries

- [CTEs make SQL more readable and are more performant](http://www.alisa-in.tech/sql/2019/10/02/ctes.html)
- Use CTEs to reference other tables. Think of these as import statements
- CTEs should be placed at the top of the query
- Where performance permits, CTEs should perform a single, logical unit of work
- CTE names should be as verbose as needed to convey what they do
- CTEs with confusing or noteable logic should be commented in file and documented in dbt docs
- CTEs that are duplicated across models should be pulled out into their own models
- Leave an empty row above and below the query statement
-    CTEs should be formatted as follows:

     ``` sql
     WITH events AS ( -- think of these select statements as your import statements.
     
       ...
     
     ), filtered_events AS ( -- CTE comments go here
     
       ...
     
     ) 
     
     SELECT * -- you should always aim to "select * from final" for your last model
     FROM filtered_events
     ```

##### General
 
- Indents should be two spaces (except for predicates, which should line up with the `WHERE` keyword)
- Lines of SQL should be no longer than 80 characters
- When `SELECT`ing, always give each column its own row, with the exception of `SELECT *` which can be on a single row
- `DISTINCT` should be included on the same row as `SELECT`
- The `AS` keyword should be used when projecting a field or table name
- When aliasing use `AS`, strive to align the original column names on a single vertical line and the `AS` keyword on a separate vertical line 
- Fields should be stated before aggregates / window functions
- Ordering and grouping by a number (eg. group by 1, 2) is preferred
- Prefer `WHERE` to `HAVING` when either would suffice
- Prefer accessing JSON using the bracket syntax, e.g. `data_by_row['id']::bigint as id_value`
- **Never** use `USING` in joins. On Snowflake, it will produce inaccurate results  
- Prefer `UNION ALL` to `UNION`. This is because a `UNION` could indicate upstream data integrity issue that are better solved elsewhere. 
- Prefer `!=` to `<>`. This is because `!=` is more common in other programming languages and reads like "not equal" which is how we're more likely to speak
- Consider performance. Understand the difference between `LIKE` vs `ILIKE`, `IS` vs `=`, and `NOT` vs `!` vs `<>`. Use appropriately
- Prefer `lower(column) LIKE '%match%'` to `column ILIKE '%Match%'`. This lowers the chance of stray capital letters leading to an unexpected result
- Prefer using `VARCHAR` when casting this data type and not specifying a length. A column only consumes storage for the amount of actual data stored. 
- Familiarize yourself with [the DRY Principal](https://docs.getdbt.com/docs/design-patterns). Leverage CTEs, jinja and macros in dbt, and snippets in Periscope. If you type the same line twice, it needs to be maintained in two places 
- **DO NOT OPTIMIZE FOR A SMALLER NUMBER OF LINES OF CODE. NEWLINES ARE CHEAP. [BRAIN TIME IS EXPENSIVE.](https://blog.getdbt.com/write-better-sql-a-defense-of-group-by-1/)**

##### Functions

- Function names and keywords should all be capitalized 
- Prefer `NULLIF` TO `NVL`  
- Prefer `IFF` to a single line `CASE WHEN` statement

##### JOINs
- Be explicit when joining, e.g. use `LEFT JOIN` instead of `JOIN`. (Default joins are `INNER`) 
- Prefix the table name to a column when joining, otherwise omit
- Specify the order of a join with the FROM table first and JOIN table second:

     ```sql
     -- Good
     FROM source
     LEFT JOIN other_source 
       ON source.id = other_source.id
     WHERE ...
     
     -- Bad
     FROM source
     LEFT JOIN other_source 
       ON other_source.id = source.id
     WHERE ...
     ```

 

##### Example Code

* Putting it all together:

     ```sql
     WITH my_data AS (
     
         SELECT *  
         FROM analytics.my_data
         WHERE filter = 'my_filter'
     
     ), some_cte AS (
     
         SELECT DISTINCT
           id,
           other_field_1,
           other_field_2
         FROM analytics.my_other_data
     
     )
     
     SELECT 
       data_by_row['id']::bigint  AS id_field,
       field_1                    AS detailed_field_1,
       field_2                    AS detailed_field_2,
       detailed_field_3,
       CASE 
         WHEN cancellation_date IS NULL AND expiration_date IS NOT NULL
           THEN expiration_date
         WHEN cancellation_date IS NULL
           THEN start_date+7
         ELSE cancellation_date
       END                        AS cancellation_date,
       SUM(field_4)               AS field_4_sum,
       MAX(field_5)               AS field_5_max
     FROM my_data
     LEFT JOIN some_cte 
       ON my_data.id = some_cte.id 
     WHERE field_1 = 'abc'
       AND (field_2 = 'def' OR field_2 = 'ghi')
     GROUP BY 1, 2, 3, 4
     HAVING COUNT(*) > 1
     ORDER BY 4 DESC
     ```

##### Commenting

* When making single line comments in a model use the `--` syntax
* When making multi-line comments in a model use the `/*  */` syntax
* Respect the character line limit when making comments. Move to a new line or to the model documentation if the comment is too long
* dbt model comments should live in the model documentation
* Calculations made in SQL should have a brief description of what's going on and a link to the handbook defining the metric (and how it's calculated)
* Instead of leaving `TODO` comments, create new issues for improvement

### dbt Guidelines

##### General

- Organize models, tests, macros, and snapshots into logical folders based on source. Use analysis type folders when logical (i.e. retention, smau)
-    Follow the naming convention of `analysis type, data source (in alpha order, if multiple), thing, aggregation` 

     ```sql
     -- Good
     retention_sfdc_zuora_customer_count.sql
     
     -- Bad
     retention.sql
     ```

- All `{{ ref('...') }}` statements should be placed in CTEs at the top of the file. (Think of these as import statements.)
  - This does not imply all CTE's that have a `{{ ref('...') }}` should be `SELECT *` only. It is ok to do additional manipulations in a CTE with a `ref` if it makes sense for the model

##### Sources

-    When working with source tables with names that don't meet our usual convention or have unclear meanings, Use identifiers to override source table names when the original is messy or confusing. ([Docs on using identifiers](https://docs.getdbt.com/docs/using-sources#section-configuring-sources))

     ```yaml
     # Good
     tables: 
       - name: bizible_attribution_touchpoint
         identifier: bizible2__bizible_attribution_touchpoint__c
     
     # Bad
     tables: 
       - name: bizible2__bizible_attribution_touchpoint__c
     ```

##### Base Models

- Only base models should select from source tables
- Base models should not select from the `raw` database directly. Instead, they should reference sources, e.g. `FROM {{ source('bamboohr', 'job_info') }}`
- Only a single base model should be able to select from a given source table
- Base models should be placed in a `base/` directory
- Base models should perform all necessary data type casting, using the `::` sytax when casting (You accomplish the same thing with fewer characters, and it presents as cleaner). 
  - Ideally, base models should cast every column. Explicit is better than implicit. Test your assumptions
- Base models should perform all field naming to force field names to conform to standard field naming conventions
- Source fields that use reserved words must be renamed in base models
- Base models for particularly large data should always end with an ORDER BY statement on a logical field (usually a relevant timestamp). This essentially defines the cluster key for the warehouse and will help to take advantage of [Snowflake's micro-partitioning](https://docs.snowflake.net/manuals/user-guide/tables-clustering-micropartitions.html).
- New fields should be generated as close to the source as possible and added to the base model. This makes it easier for the day when data may be available in the source system, in which case we could remove the generator in the base model and just pull in that additional field.

###### Organizing columns

When writing a base model, colummns should have some logical ordering to them. 
We encourage these 4 basic groupings:
  - Primary data
  - Foreign keys
  - Logical data - This group can be subdivided further if needed
  - Metadata

Primary data is the key information describing the table. The primary key should be in this group along with other relevant unique attributes such as name.

Foreign keys should be all the columns which point to another table.

Logical data is for additional data dimensions that describe the object in reference. For a Salesforce opportunity this would be the opportunity owner or contract value. Further logical groupings are encouraged if they make sense. For example, having a group of all the variations of contract value would make sense. 

Within any group, the columns should be alphabetized on the alias name.

An exception to the grouping recommendation is when we control the extraction via a defined manifest file. A perfect example of this is our [gitlab.com manifest](https://gitlab.com/gitlab-data/analytics/blob/master/extract%2Fpostgres_pipeline%2Fmanifests%2Fgitlab_com_db_manifest.yaml) which defines which columns we extract from our application database. The base models for these tables can be ordered identically to the manifest as it's easier to compare diffs and ensure accuracy between files.

-   Ordered alphabetically by alias within groups

     ```sql
     -- Good 
     
     SELECT
       id                    AS account_id,
       name                  AS account_name,
     
       -- Foreign Keys
       ownerid               AS owner_id,
       pid                   AS parent_account_id,
       zid                   AS zuora_id,
     
       -- Logical Info
       opportunity_owner__c  AS opportunity_owner,  
       account_owner__c      AS opportunity_owner_manager,
       owner_team_o__c       AS opportunity_owner_team,
     
       -- metadata
       isdeleted             AS is_deleted,
       lastactivitydate      AS last_activity_date
     FROM table
     ```

-   Ordered alphabetically by alias without groups

     ```sql
     -- Less Good 
     
     SELECT
       id                    AS account_id,
       name                  AS account_name,
       isdeleted             AS is_deleted,
       lastactivitydate      AS last_activity_date,
       opportunity_owner__c  AS opportunity_owner,  
       account_owner__c      AS opportunity_owner_manager,
       owner_team_o__c       AS opportunity_owner_team,
       ownerid               AS owner_id,
       pid                   AS parent_account_id,
       zid                   AS zuora_id
     FROM table
     ```

-   Ordered alphabetically by original name

     ```sql
     -- Bad 
     
     SELECT
       account_owner__c      AS opportunity_owner_manager,
       id                    AS account_id,
       isdeleted             AS is_deleted,
       lastactivitydate      AS last_activity_date
       name                  AS account_name,
       opportunity_owner__c  AS opportunity_owner,  
       owner_team_o__c       AS opportunity_owner_team,
       ownerid               AS owner_id,
       pid                   AS parent_account_id,
       zid                   AS zuora_id
     FROM table
     ```

##### Testing
- Every model should be tested in a `schema.yml` file
- At minimum, unique, not nullable fields, and foreign key constraints should be tested (if applicable)
- The output of dbt test should be pasted into MRs
- Any failing tests should be fixed or explained prior to requesting a review

### Other SQL Style Guides 
- [Fishtown Analytics](https://github.com/fishtown-analytics/corp/blob/master/dbt_coding_conventions.md#sql-style-guide)
- [Matt Mazur](https://github.com/mattm/sql-style-guide)
- [Kickstarter](https://gist.github.com/fredbenenson/7bb92718e19138c20591)

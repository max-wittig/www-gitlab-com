---
layout: markdown_page
title: "IT Help"
---
# Welcome to the IT Help Handbook

The IT Help team is part of the [GitLab Business Ops](/handbook/business-ops) function that guides systems, workflows and processes and is a singular reference point for operational management.

## GET IN TOUCH

* [IT Help Issues](https://gitlab.com/groups/gitlab-com/business-ops/bizops-IT-help/-/issues)
* #it-help on slack and the #it_help channel

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement

IT Help will triage all IT related questions as they arise. Build a knowledge base of IT practices and pragmatic problem solving in the handbook. Account management for password resets and lockout. On call support for immediate software and hardware issues during local business hours. Diagnose computer errors and provide technical support. Troubleshoot software and hardware. Support Weekly IT Onboarding Sessions for new Team Members. Train end-users how to setup and use new technologies. Provide technical support over the phone or Web. Use specialized help desk support software to take control of end-users' computers to troubleshoot, diagnose and resolve complex issues.


## Requests for support (2fa, password resets, etc)

Request for support should have an issue open at [IT Help Issues](https://gitlab.com/groups/gitlab-com/business-ops/bizops-IT-help/-/issues). Certain support items like 2FA resets are time sensitive but there are some things you can do to help yourself. For 2FA related problems for your Gitlab accounts, please use your backup codes or try generating [new ones](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh).

Follow these steps to successfully set up 2fa for your [Google account](https://support.google.com/accounts/answer/185839?co=GENIE.Platform%3DDesktop&hl=en).

When you get a new device, before you get rid of the old one, make sure to set up your authenticator app on the new device. You will need to log into your accounts with the old device and disable 2fa for your accounts. Then delete the accounts off the old device and re-enable the 2fa on your accounts on the authenticator app on the new device. If you still need a 2fa reset, please reach out to #it_help on slack with the system that needs it and the issue you are encountering. 


We understand sometimes life happens and passwords get forgotten or deleted. If it is for a system that requires immediate access, please reach out on the slack #it_help channel. Provide as much information as possible. Password resets to sensitive systems such as your G Suite account and Okta require a zoom call to verify. 

As a distributed team, our current standard coverage window is ~13:00-22:00 UTC. High volumes of issues being triaged can dictate the delay in response within that window. If the issue is extremely time sensitive and warrants escalation, use judgement on whether or not it can wait until ‘business hours’. Escalated issues should be made through the #it_help slack channel. All other request should have an issue created. 


### How to contact us, or escalate priority issues outside of standard hours:


Slack is not ideal for managing priorities of incoming issues, so we ask that all such requests get created at [IT Help Issues](https://gitlab.com/groups/gitlab-com/business-ops/bizops-IT-help/-/issues) and use [THIS](https://gitlab.com/gitlab-com/business-ops/bizops-IT-help/hd-issue-tracker/issues/new?issuable_template=General%20HelpDesk%20Request) template for general issues. We will triage and address them as soon as we can.  All issues created in the queue are public by default.

Privileged or private communications should be sent to it-help@. 

Screenshots and videos are very helpful when experiencing an issue, especially if there is an error message.


### Instructions on how to create Role-Based Entitlement Templates.

Below is a summary of how to create new templates for a Role-Based Entitlement that does not have one. 

1. pick a role that is somewhat similar to or look through the templates that already exist. [Issue Template](https://gitlab.com/gitlab-com/access-requests/tree/master/.gitlab/issue_templates)

2. then copy the markdown and create a new file in the directory. [Acess Request](https://gitlab.com/gitlab-com/access-requests/tree/master/.gitlab/issue_templates)

3. name it with _ instead of any spaces with the dept at the beginning like Eng_Backend_Engineer_Entitlements_Access_Request.

4. update the copy to reflect their job title, systems, edit the bottom copy of tech stack provisioners and labels. (which you can assist with)

5. once it’s prepared, it will need to be reviewed by a manager and director in their department.

6. then if the management comments their approval, it can be merged.

7. add to README and AR handbook page.


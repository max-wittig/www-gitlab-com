---
layout: default
title: Continuous integration and delivery
description: "Learn more about continuous software development with GitLab CI/CD."
suppress_header: true
extra_css:
  - devops.css
  - cicd.css
extra_js:
  - libs/on-scroll.js
  - in-page-nav.js
  - all-clickable.js
canonical_path: "/ci-cd/"
---
.blank-header
  %img.image-border.image-border-left{ src: "/images/home/icons-pattern-left.svg", alt: "Gitlab hero border pattern left svg" }
  %img.image-border.image-border-right{ src: "/images/home/icons-pattern-right.svg", alt: "Gitlab hero border pattern right svg" }
  .header-content
    %img.hero-image-huge{ src: "/images/cicd/g_gitlab-ci-cd.svg", alt: "Gitlab devops loop svg" }
    %h1 Continuous integration and delivery
    %p Learn more about continuous software development with GitLab CI/CD.
    %a.btn.cta-btn.orange.devops-cta{ href: "/free-trial/?utm_medium=pdf&utm_source=ebook&utm_campaign=cicdcmp1&utm_content=10advantagesofcicd" } Watch a GitLab CI/CD webcast

.wrapper.wrapper--large-fonts
  #content.devops-content.u-margin-top-lg
    .wrapper.container.js-in-page-nav-group{role: "main"}
      .row.u-margin-top-lg.js-in-page-nav-section#what-is-ci-cd
        .col-md-10.col-md-offset-1
          %h2.u-text-brand What is CI/CD?

          %p Continuous integration (CI) and continuous delivery (CD) enables DevOps teams to deliver better quality code, faster. Continuous integration works to integrate code from your team in a shared repository. Developers share their new code in a Merge (Pull) Request, which triggers a pipeline to build, test, and validate the new code before merging the changes in your repository. Continuous delivery deploys CI-validated code to your application.

          %p Code is tested throughout each stage, ensuring better quality builds and applications with fewer bugs. Pipelines can determine what happens when builds pass or fail these tests, meaning that errors are identified much faster. As code goes through each stage, it&rsquo;s continually validated against many other changes in the repository happening concurrently, which ensures code integrity throughout the pipeline. Together, CI and CD accelerate how quickly your team delivers results for your customers and stakeholders.

          %blockquote.blockquote-alt.cicd-blockquote
            %b Continuous Integration 
            is the practice of integrating code into a shared repository and building/testing each change automatically, as early as possible &ndash; usually several times a day.

          %blockquote.blockquote-alt.cicd-blockquote
            %b Continuous Delivery 
            ensures CI-validated code can be released to production at any time.

          %p Continuous delivery is often used interchangeably with continuous deployment, but there is a subtle difference between the two. Continuous deployment means that all validated code deploys to production automatically, whereas continuous delivery means that this code _can_ be deployed. The flexibility for code to deployed at any time is what differentiates delivery from deployment, and continuous deployment is possible when continuous delivery is already in place.

      .row.u-margin-top-xl.js-in-page-nav-section#benefits-of-ci-cd
        .col-md-10.col-md-offset-1
          %h2.u-text-brand Benefits of CI/CD

          %p CI/CD automates workflows and reduces error rates, which can have far-reaching impacts throughout an organization.

          %ul
            %li More time for innovation
            %li Better retention rates
            %li More revenue
            %li Business efficiency

          %p For example, a dev environment with less manual tasks means that engineers can spend more time on revenue-generating projects. With fewer errors, teams are more efficient and spend less time putting out fires. When processes are automated, engineers are happier and can focus on where they add the most value.

          %p CI/CD brings automation into the DevOps lifecycle. With less manual work, DevOps teams work more efficiently and with greater speed. An automated workflow also reduces the chance of human error and improves handoffs, which improves overall operational efficiency. Organizations that implement CI/CD make better use of their resources and will have a competitive edge over those that don't use CI/CD.

      .row.u-margin-top-xl.js-in-page-nav-section#ci-cd-fundamentals
        .col-md-10.col-md-offset-1
          %h2.u-text-brand CI/CD fundamentals

          %p CI/CD is all about efficiency and is built around fundamental elements to make it effective.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px" }
                %img{src: "/images/icons/first-look-influence.svg"}

            .media-body.media-middle
              %h3.u-margin-top-0 A single source repository

              %p.u-margin-bottom-0 Source code management (SCM) that houses all necessary files and scripts to create builds.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                %img{src: "/images/icons/build.svg"}
            .media-body.media-middle
              %h3.u-margin-top-0 Automated builds

              %p.u-margin-bottom-0 Scripts should include everything you need to build from a single command.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                %img{src: "/images/icons/computer-test.svg"}
            .media-body.media-middle
              %h3.u-margin-top-0 Builds should be self-testing

              %p.u-margin-bottom-0 Testing scripts should ensure that the failure of a test should result in a failed build.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                %img{src: "/images/icons/scale.svg", style: "height: 80px;"}
            .media-body.media-middle
              %h3.u-margin-top-0 Frequent iterations

              %p.u-margin-bottom-0 Multiple commits to the repository mean there are fewer places for conflicts to hide.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                %img{src: "/images/icons/stable-computer.svg"}
            .media-body.media-middle
              %h3.u-margin-top-0 Stable testing environments

              %p.u-margin-bottom-0 Code should be tested in a cloned version of the production environment.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                %img{src: "/images/icons/visibility.svg", style: "height: 80px;"}
            .media-body.media-middle
              %h3.u-margin-top-0 Maximum visibility

              %p.u-margin-bottom-0 Every developer should be able to access the latest executables and see any changes made to the repository.

          .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
            .media-left.media-middle.u-padding-right-sm
              .media-object{ style: "width: 80px; height: 80px;" }
                %img{src: "/images/icons/gitlab-rocket.svg"}
            .media-body.media-middle
              %h3.u-margin-top-0 Automated deployments

              %p.u-margin-bottom-0 Code should be able to deploy into multiple environments easily.

      .row.u-margin-top-md.js-in-page-nav-section#why-gitlab-ci-cd
        .col-md-10.col-md-offset-1
          %h2.u-text-brand Why Gitlab CI/CD?

          %p In order to complete all the required fundamentals of CI/CD, many CI platforms rely on integrations with other tools to fulfill those needs. Many organizations have to maintain costly and complicated toolchains in order to have full CI/CD capabilities. This often means maintaining a separate SCM like Bitbucket or GitHub, connecting to a separate testing tool, that connects to their CI tool, that connects to a deployment tool like Chef or Puppet, that also connects to various security and monitoring tools.

          %p Instead of just focusing on building great software, organizations have to also maintain and manage a complicated toolchain. GitLab is a single application for the entire DevOps lifecycle, meaning we fulfill all the fundamentals for CI/CD in one environment.

          %div.u-image-bg.u-release.u-text-light.u-margin-top-xl.u-margin-bottom-md{ style: "background-image:url('/images/scaling-devops-bg.jpg');" }
            .container
              .row.u-margin-top-lg.u-margin-bottom-lg
                .col-md-10.col-md-offset-1
                  %h2 GitLab CI/CD rated #1 

                  %h3.u-margin-top-md Build, test, deploy, and monitor your code from a single application.
                  %p We believe a single application that offers visibility across the entire SDLC is the best way to ensure that every development stage is included and optimized. When everything is under one roof, it’s easy to pinpoint workflow bottlenecks and evaluate the impact each element has on deployment speed. GitLab has CI/CD built right in, no plugins required.

                  .btn-group
                    %a.btn.cta-btn.purple-reverse{ href: "/product/continuous-integration/" } Explore GitLab CI/CD


          .feature-group.feature-group--alt.u-margin-top-sm.u-padding-top-0.u-padding-bottom-0
            .row.flex-row
              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-gitlab-cicd.png", alt: 'DevOps', srcset: "/images/feature-thumbs/feature-thumb-gitlab-cicd_2x.png 2x"
                  .feature-body
                    %h3.feature-title Why Gitlab CI/CD
                    %p.feature-description With GitLab’s out-of-the-box CI/CD, you can spend less time maintaining and more time creating.
                    %a.feature-more{ href: "/2019/04/02/why-gitlab-ci-cd/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-beginners-guide-cicd.png", alt: 'Create a CI/CD pipeline blog post', srcset: "/images/feature-thumbs/feature-thumb-beginners-guide-cicd_2x.png 2x"
                  .feature-body
                    %h3.feature-title A beginner's guide to continuous integration
                    %p.feature-description Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
                    %a.feature-more{ href: "/2018/01/22/a-beginners-guide-to-continuous-integration/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-gitlab-logo-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-gitlab-logo-cicd_2x.png 2x"
                  .feature-body
                    %h3.feature-title 5 teams that made the switch to GitLab
                    %p.feature-description See what happened when these five teams moved on from old continuous integration and delivery solutions and switched to GitLab CI/CD.
                    %a.feature-more{ href: "/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-why-gitlab-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-why-gitlab-cicd_2x.png 2x"
                  .feature-body
                    %h3.feature-title 4 benefits of CI/CD
                    %p.feature-description How to measure a successful CI/CD strategy.
                    %a.feature-more{ href: "/2019/06/27/positive-outcomes-ci-cd/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-4-benefits-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-4-benefits-cicd_2x.png 2x"
                  .feature-body
                    %h3.feature-title The business impact of CI/CD
                    %p.feature-description How a good CI/CD strategy generates revenue and keeps developers happy.
                    %a.feature-more{ href: "/2019/06/21/business-impact-ci-cd/" } Read

              .col-md-4.col-lg-4.u-margin-bottom-sm
                .feature.js-all-clickable
                  .feature-media
                    = image_tag "/images/feature-thumbs/feature-thumb-frontend-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-frontend-cicd_2x.png 2x"
                  .feature-body
                    %h3.feature-title How DevOps and GitLab CI/CD enhance a frontend workflow
                    %p.feature-description The GitLab frontend team uses DevOps and CI/CD to ensure code consistency, fast delivery, and simple automation.
                    %a.feature-more{ href: "/2018/08/09/how-devops-and-gitlab-cicd-enhance-a-frontend-workflow/" } Read


      .row.u-margin-top-xl.js-in-page-nav-section#resources
        .col-md-10.col-md-offset-1
          %h2.u-text-brand Resources


          %p Here’s a list of resources on CI/CD that we find to be particularly helpful in understanding CI/CD and implementation. We would love to get your recommendations on books, blogs, videos, podcasts and other resources that tell a great CI/CD story or offer valuable insight on the definition or implementation of the practice.

          %p
            Please share your favorites with us by tweeting us
            %a{:href => 'https://twitter.com/gitlab'} @GitLab!

          .resource-block.resource-block--webcasts.u-margin-top-md
            %h3.block-title Webcasts

            %ul.resource-list.list-unstyled
              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/webcast/mastering-ci-cd/" } Mastering continuous software development


          %hr.u-margin-top-md.u-margin-bottom-md


          .resource-block.resource-block--blogs
            %h3.block-title Whitepapers and ebooks

            %ul.resource-list.list-unstyled
              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/resources/whitepaper-scaled-ci-cd/" } Scaled continuous integration and delivery
                  
            %ul.resource-list.list-unstyled
              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/resources/ebook-single-app-cicd/" } The benefits of single application CI/CD


          %hr.u-margin-top-md.u-margin-bottom-md


          .resource-block.resource-block--podcasts
            %h3.block-title Case studies

            %ul.resource-list.list-unstyled
              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/customers/cncf/" } Cloud Native Computing Foundation (CNCF)

              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/2017/06/07/continous-integration-ticketmaster/" } Ticketmaster

              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/2019/02/14/verizon-customer-story/" } Verizon


          %hr.u-margin-top-md.u-margin-bottom-md


          .resource-block.resource-block--podcasts
            %h3.block-title Reports

            %ul.resource-list.list-unstyled
              %li.resource-list-item
                %h4.resource-title
                  %a{ href: "/resources/forrester-wave-ci-2017/" } Voted #1 in The Forrester Wave<sup>TM</sup>: Continuous Integration Tools, Q3 2017
